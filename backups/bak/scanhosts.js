/** @param {NS} ns **/
//SOLID
import { serverinfo, loopinstall } from "settings.js";
export async function main(ns) {

	//var scriptram = ns.getScriptRam(settings.scripts.weak, "home");

	var recursion = 0;

	var opts = {
		target: "temp",
		appendonly: true,
		verbose: false,
		silent: false,
		getinfo: false,
		hackable: false,
		exclude: "", //search string to exclude
		includeonly: "",
		grep: "",
	}

	//var target = "temp";

	var usage = "Usage: {flags}\n\
		[-h]		This help text\n\
		[target]		Target for hacking scripts\n\
		[-r recursion]	Number of levels to scan. Default is max.\n\
		[--hackable]	Show/scan only hackable servers\n\
		[-f grep]		Find files with pattern grep\n\
		[--replace]		Re-up new script to running servers\n\
		[-e exclude]	Partial name of servers to EXCLUDE\n\
		[-i include]	Partial name of servers to INCLUDE\n\
		[--info]		Get server information\n\
		[-v]		make it verbose\n\
		[-q]		make it silent\n\n";

	// parse command line arguments
	var lines = ns.args;
	while (lines.length > 0) {
		let item = lines.shift();
		switch (item) {
			case "--replace":
				opts.appendonly = false;
				break;
			case "-r":
				opts.recursion = lines.shift();
				break;
			case "-e": // partial names of servers to exclude
				opts.exclude = lines.shift();
				break;
			case "-f": // partial names of servers to exclude
				opts.grep = lines.shift();
				break;
			case "-i": // include
				opts.includeonly = lines.shift();
				break;
			case "-v":
				opts.verbose = true;
				break;
			case "-q":
				opts.silent = true;
				break;
			case "--hackable":
				opts.hackable = true;
				break;
			case "--info":
				opts.getinfo = true;
				break;
			case "-h":
			default:
				if (item.match(/^-/)) {
					// bad argument
					opts.target = ""; // this forces script to fail
				} else {
					// un-tacked argument defaults to target name
					opts.target = ns.serverExists(item) ? item : "";
				}
				break;
		}
	}


	if (!opts.target) {
		ns.tprint(usage);
		ns.exit();
	}

	if (ns.serverExists(opts.target)) {
		if (ns.getHackingLevel() >= ns.getServerRequiredHackingLevel(opts.target)) {
			//await installhack(opts.target, opts.target);
			await loopinstall(ns, opts.target, opts);
			//make sure we also use HOME
			// await installhack("home", opts.target);
			await loopinstall(ns, "home", opts);
		} else {
			ns.tprint("Target " + opts.target + " hacking level too high "
				+ ns.getServerRequiredHackingLevel(opts.target));
			ns.exit();
		}
	}

	await scanhost("home", "");

	/// END OF MAIN SCRIPT ///

	if (opts.verbose) {
		ns.tprint("-- INSTALLATION COMPLETE --");
		ns.tprint("-- Target: " + opts.target + " --");
	}

	/// FUNCTIONS BELOW ///

	// SCAN and do things
	async function scanhost(host, parent, recursioncount = 1) {
		//await ns.sleep(20); // prevent runaway script
		//ns.tprint((" ").repeat(indentcount), "SCANNING host: ", host, " ", recursioncount);
		let servers = ns.scan(host); // get servers connected to host
		for (const server of servers) {

			// skip the server we came from
			// if parent is the only connection this stops the recursion
			if (server == parent) continue;

			// skip purchased servers
			//if (server.match("pserv")) continue;

			// exclude filter
			if (opts.exclude && server.match(opts.exclude)) continue;

			// include-only filter
			if (opts.includeonly && !server.match(opts.includeonly)) continue;

			// found a connected server. DO THINGS
			let serverhacklevel = ns.getServerRequiredHackingLevel(server);

			if (opts.hackable && ns.getHackingLevel() < serverhacklevel) continue;

			if (!opts.silent) ns.tprint(("-").repeat(recursioncount), server,
				" (", serverhacklevel, ") ", recursioncount);


			if (opts.getinfo) {
				//ns.tprint("getinfo for" + server);
				let info = ns.getServer(server);
				ns.tprint(serverinfo(info))
				//ns.exec("check-getinfo.js", "home", 1, server);
			} else if (opts.grep) {
				// looking for server files
				await listfiles(server, opts.grep, recursioncount);

			} else if (ns.serverExists(opts.target)) {
				// installing hack scripts
				//ns.tprint(server + " , " + target); await ns.sleep(5);
				if (opts.verbose) ns.tprint("-------- Installhacking " + server);
				await loopinstall(ns, server, opts);
				//await installhack(server, opts.target); //, recursioncount);
				//await ns.sleep(100);
			} else {
				//ns.tprint((" ").repeat(recursioncount) + "Server does not exist?? " + target);
				//ns.tprint("WHAT IS HAPPENING");
			}

			// continue recursive scanning
			if (recursion == 0 || recursion > recursioncount) {
				await scanhost(server, host, recursioncount + 1);
			}
		}

	}


	async function listfiles(server, grep, indentcount) {
		let files = ns.ls(server, grep);
		//if (files.length == 0) return;
		//ns.tprint((" ").repeat(indentcount), ": ", server, ":");
		while (files.length > 0) {
			ns.tprint((" ").repeat(indentcount), files.shift());
		}
	}

/*
	async function installhack(server, target, indentcount = 1) {
		let installscript = "installhack.js";
		let freeram = ns.getServerMaxRam("home") - ns.getServerUsedRam("home");
		let scriptram = ns.getScriptRam(installscript, "home");
		while (ns.scriptRunning(installscript, "home") && freeram < scriptram) {
			//hang on a minute if we're temp out of ram
			await ns.sleep(500);
		}
		try {
			let args = ["-s", server, target];
			if (!appendonly) args.push("--replace");
			if (verbose) args.push("-v");
			if (silent) args.push("-q");
			ns.exec(installscript, "home", 1, ...args);
			await ns.sleep(100);
		} catch (e) { ns.tprint("Installhack error: " + e); }
	}
*/
}