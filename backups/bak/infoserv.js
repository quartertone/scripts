/** @param {NS} ns **/
 import { number } from "settings.js";
export async function main(ns) {
ns.tprint("2 ** " + ns.args[0] 
+ " = " + 2**ns.args[0] + " »» " + number(ns.getPurchasedServerCost(2**ns.args[0])));
}