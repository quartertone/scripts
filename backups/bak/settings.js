/** @param {NS} ns **/

export var settings = {
	//target: "phantasy",
	host: "home",
	//	ram: 4096,
	script: "hack.js",
	scripthack: "loop-hack.js",
	scriptgrow: "loop-grow.js",
	scriptweak: "loop-weaken.js",

	//	append: true, // don't delete/upload
	//	recursion: 1, // number of levels to hack

	// for redditstock.js
	threshold: 0.58, // min forecast threshold
	moneykeep: 100000000, //100 mil
	volthresh: 0.05, // max allowable volatility
	minshares: 5, //minimum number of shares to buy
	minbuy: 1000000, // minimum purchase 1 mil
}

export function number(num) {
	let exp = Math.log10(num);
	// 12 = tril
	// 9 = bil
	// 6 = mil
	// 8 = k
	if (exp >= 12) {
		return (num / (10 ** 12)).toFixed(3) + "t";
	} else if (exp >= 9) {
		return (num / (10 ** 9)).toFixed(3) + "b";
	} else if (exp >= 6) {
		return (num / (10 ** 6)).toFixed(3) + "m";
	} else if (exp >= 3) {
		return (num / (10 ** 3)).toFixed(3) + "k";
	} else {
		return num.toFixed(2);
	}
}