/** @param {NS} ns **/
export async function main(ns) {

	var sym = "";
	var commfee = 100000;
	var threshold = 0.65;
	//var getinfo = false;
	var makemoney = false;
	var sellonly = false;

	var lines = ns.args;
	while (lines.length > 0) {
		let item = lines.shift();
		switch (item) {
			case "-m":
				makemoney = true; // Make money!
				break;
			case "-s":
				//sell only
				sellonly = true;
				break;
			case "-l":
				threshold = lines.shift();
				break;
			default:
				if (item.match(/^-/)) {
					// bad argument
					sym = ""; // this forces script to fail
				} else {
					// un-tacked argument defaults to target name
					sym = item;
				}
				break;
		}
	}

	if (!sym || sym == "ALL") {
		let symbols = ns.stock.getSymbols();
		while (symbols.length > 0) {
			let sym = symbols.shift();
			if (ns.stock.getForecast(sym) > threshold) getstockinfo(sym);
		}
		ns.tprint(" to MAKE MONEY, use flag '-m'");
	} else {
		//		for (let i=0; i<2) (true) {
		let forecast = 0;
		let purchaseprice = 0;
		let shares = 0;
		if (!sellonly) {
			while ((forecast = ns.stock.getForecast(sym)) < threshold) {
				// wait until favorable forecast
				ns.tprint("checking ", sym,
					": poor forecast:", forecast.toFixed(4),
					" price: ", ns.stock.getAskPrice(sym).toFixed(2),
				);
				await ns.sleep(2500);
				//TODO - break out if it's been too long
			} // we have a good forecast

			// this is a fail-safe, but the while loop should hold up
			let goodforecast = ns.stock.getForecast(sym) < threshold ? true : false;

			// calculate max shares purchasable
			shares = Math.floor(
				(ns.getServerMoneyAvailable("home") - (commfee * 2))
				/ ns.stock.getAskPrice(sym));

			ns.tprint(sym, "\n Forecast: ", forecast.toFixed(4),
				"\n can buy max shares: ", shares,
				" at ", ns.stock.getAskPrice(sym).toFixed(2),
			);

			// Purchaseprice includes commission
			purchaseprice = ns.stock.getPurchaseCost(sym, shares, "Long");
			if (makemoney) {
				let avgprice = ns.stock.buy(sym, shares);
				ns.tprint("BOUGHT ", shares, " shares of ", sym,
					" at ", avgprice, " (listed at ", ns.stock.getAskPrice(sym).toFixed(2),
					" total: ", (avgprice * shares).toFixed(2),
					" (calculated: ", purchaseprice.toFixed(2), ") ",
					"\n==",
				);
			} else {
				ns.tprint("= fake Buying ", shares, " shares of ", sym,
					" at ", ns.stock.getAskPrice(sym).toFixed(2),
					" total: ", purchaseprice.toFixed(2),
					"\n==",
				);
			}
		}
		//bought stock. now watch the price
		//waiting to sell
		let position = ns.stock.getPosition(sym);
		if (sellonly) {
			purchaseprice = position[0] * position[1];
			shares = position[0];
		}
		let salegain = 0;
		while ((salegain = ns.stock.getSaleGain(sym, shares, "Long")) < (purchaseprice + commfee * 2)) {
			ns.tprint("forecast: ", ns.stock.getForecast(sym).toFixed(4),
				" bid: ", ns.stock.getBidPrice(sym).toFixed(2),
				" - current profit: ",
				salegain.toFixed(2), " - ", ((commfee * 2) + purchaseprice).toFixed(2),
				" = ", (salegain - purchaseprice).toFixed(2),
				//"\n.",
			);
			await ns.sleep(2500);
		}

		if (makemoney || sellonly) {
			let avgprice = ns.stock.sell(sym, shares);
			ns.tprint("SOLD ", shares, " shares of ", sym,
				" at ", avgprice, " (listed at ", ns.stock.getBidPrice(sym).toFixed(2),
				" profit: ", (avgprice * shares).toFixed(2),
				" (calculated: ", salegain.toFixed(2), " - ", purchaseprice.toFixed(2),
				" = ", (salegain - purchaseprice).toFixed(2), ") ",
			);
		} else {
			ns.tprint("Selling",
				"\n forecast: ", ns.stock.getForecast(sym).toFixed(4),
				" bid: ", ns.stock.getBidPrice(sym).toFixed(2),
				"\n profit:",
				salegain.toFixed(2), " - ", ((commfee * 2) + purchaseprice).toFixed(2),
				" = ", (salegain - purchaseprice).toFixed(2),
			);
		}
		ns.tprint("End of Makemoney loop");
		//	}
	}

	function getstockinfo(sym) {

		ns.tprint(
			sym, ":",
			"\tprice: ", ns.stock.getAskPrice(sym).toFixed(0),
			//"\n max shares: ", ns.stock.getMaxShares(sym),
			"\t  forecast: ", ns.stock.getForecast(sym).toFixed(4),
			"\tvolat: ", (100 * ns.stock.getVolatility(sym)).toFixed(2), "%",
			//"\n",
			//"\n can buy max shares: ",
			//Math.floor((mymoney / ns.stock.getAskPrice(sym)))
		);
	}
}