/**
 * @param {NS} ns
 **/
// https://www.designbombs.com/code-sharing-websites/

import { settings } from "settings.js";

export async function main(ns) {
	var host = settings.host;
	var grep = "";
	var recursion = settings.recursion; // recursion of 0 and 1 are the same thing

	// setup variable defaults
	var exclude = "pserv"; //search string to exclude
	var includeonly = "";
	var recursioncount = []; //need to be an array to keep track of per-host recursion count
	var indent = "";
	var indenttext = "---";

	var usage = "Usage: {flags} grep\n\
	[grep]	partial filename to search for (use \"cct\" for contracts)\n\
	[-h host]	default host = 'home'\n\
	[-r recursion]	number of levels to recurse\n\
	[-e exclude]	partial names of servers to EXCLUDE\n\
	[-i include]	partial names of servers to INCLUDE";

	// parse command line argumentsh
	var lines = ns.args;
	while (lines.length > 0) {
		let item = lines.shift();
		switch (item) {
			case "-h": // specify host; default = "home"
				host = lines.shift();
				break;
			case "-r": // recurse this many levels; default = 1
				recursion = lines.shift();
				break;
			case "-e": // partial names of servers to exclude
				exclude = lines.shift();
				break;
			case "-i": // include
				includeonly = lines.shift();
				break;
			default:
				if (item.match(/^-/)) {
					// bad argument
					grep = ""; // this forces script to fail
				} else {
					// un-tacked argument defaults to target name
					grep = item;
					console.log("grep: ", item);
				}
				break;
		}
	}
	if (grep) {
		await listfiles("home", "");
		await scanhost(host, null, null, indent);
		ns.tprint("------ The End --------");
	} else {
		// NO TARGET specified, or bad argument
		ns.tprint("usage: ", usage);
	}

	// SCAN and do things
	async function scanhost(host, ignorehost, recursionroot, indent) {
		await ns.sleep(25);
		let firstrun = !recursionroot;
		//if (verbose) ns.tprint(indent, "SCANNING host: ", host);
		let servers = ns.scan(host); // get servers connected to host
		while (servers.length > 0) {
			let server = servers.shift();
			if (server == ignorehost) continue; // don't hack if server should be ignored
			if (exclude && server.match(exclude)) continue;
			if (includeonly && !server.match(includeonly)) continue;

			if (firstrun) recursionroot = server;
			// count number of recursions for each of the first level servers
			if (recursioncount[recursionroot] == undefined) {
				recursioncount[recursionroot] = 0;
			} else {
				recursioncount[recursionroot]++;
			}
			await listfiles(server, indent);
			if (
				recursion &&
				recursioncount[recursionroot] < recursion &&
				hasdeepserver(server, host, indent)
			) {
				//ns.tprint(indent, "Recursion root = ", recursionroot);
				await scanhost(server, host, recursionroot, indent + indenttext);
			}
		}
	}

	//  check for DEEPER connection
	function hasdeepserver(nexthost, fromhost, indent) {
		let servers = ns.scan(nexthost);
		//ns.tprint(indent, "nexthost:",nexthost," fromhost:",fromhost," SERVERS:", servers);
		while (servers.length > 0) {
			let checkit = servers.shift();
			if (checkit != fromhost) {
				// ignore the higher host
				// proceed if there is a deeper connection
				ns.tprint(indent, " (", nexthost, ") -----");
				return true;
			}
		}
		return false;
	}

	async function listfiles(server, indent) {
		let files = ns.ls(server, grep);
		if (files.length == 0) return;
		ns.tprint(indent, ": ", server, ":");
		while (files.length > 0) {
			ns.tprint(indent, "   ", files.shift());
		}
	}
}