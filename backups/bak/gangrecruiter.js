/** @param {NS} ns **/
export async function main(ns) {

	// switch to WHILE 
	while (ns.gang.canRecruitMember()) {
		var members = ns.gang.getMemberNames();
		let newmember = "member-" + (members.length + 1);
		ns.tprint("Recruiting new member: " + newmember);

		ns.gang.recruitMember(newmember);
		ns.gang.setMemberTask(newmember, "Train Hacking");
		await ns.sleep(50);
	}

}