/**
 * @param {NS} ns
 **/
// https://www.designbombs.com/code-sharing-websites/

import { settings } from "settings.js";

export async function main(ns) {

	var script = settings.script;
	var scripthack = settings.scripts.hack;
	var scriptgrow = settings.scripts.grow;
	var scriptweak = settings.scripts.weak;

	var basehost = settings.host;
	var target = ""; // need to supply this on command line
	var recursion = 3; // recursion of 0 and 1 are the same thing
	var homerambuffer = 128;

	// setup variable defaults
	var dryrun = false; // actually do it by default
	var appendonly = true;
	var verbose = false;
	var oneshot = false;
	var exclude = ""; //search string to exclude
	var includeonly = "";
	//var recursioncount = []; //need to be an array to keep track of per-host recursion count
	var indent = "-";
	var indentspace = " ";
	//var indentcoun = "--";
	var hacklevels = [];

	// existence of file indicates the "on" status of this script
	var runfile = "runfile-" + ns.getScriptName() + ".txt";

	//var myhacklevel = ns.getHackingLevel();


	var usage = "Usage: {flags} target\n\
	[-h]		This help text\n\
	[target]	Name of target server (required)\n\
	[-r recursion]	Number of levels to recurse\n\n\
	[--replace]	Re-up new script to running servers\n\
	\t\t Use when changing targets\n\
	[-v]		Verbose mode; default is quiet\n\
	[-e exclude]	Partial name of servers to EXCLUDE\n\
	[-i include]	Partial name of servers to INCLUDE";

	//	[-s script]	Hack script name. Default " + scripthack + "\n\
	// parse command line argumentsh
	var lines = ns.args;
	while (lines.length > 0) {
		let item = lines.shift();
		switch (item) {
			/*case "-s": // specify script; default = "hack.script"
				script = lines.shift();
				break;
			case "--dry": // force it to go
				dryrun = true;
				break;*/
			case "--replace": // "x"out previous scripts; reload new scripts
				appendonly = false;
				break;
			case "-r": // recurse this many levels; default = 1
				recursion = lines.shift();
				break;
			case "-v": // show info about running scripts in append mode
				verbose = true; //"quiet"
				break;
			case "-e": // partial names of servers to exclude
				exclude = lines.shift();
				break;
			case "-i": // include
				includeonly = lines.shift();
				break;
			case "-o": //one-shot
				oneshot = true;
				break;
			case "-h":
			default:
				if (item.match(/^-/)) {
					// bad argument
					target = ""; // this forces script to fail
				} else {
					// un-tacked argument defaults to target name
					target = ns.serverExists(item) ? item : "";
				}
				break;
		}
	}

	if (target) {
		//if (dryrun) ns.tprint("*** DRY RUN ***");
		// check memory requirement of hacking script
		var scriptram = ns.getScriptRam(scriptweak, "home");
		ns.tprint(scripthack, " mem ", scriptram);
		// do the target first FIXME - do we need this?
		//if (host == "home") await hacktarget(target, true);

		// scanhost function does all the work
		// including scanning deeper servers (recursive)


		//prep the actual target first
		await installhack(target, "");

		// make sure we also use HOME
		await installhack("home", "");

		//ns.tprint("Deleting runfile " + runfile);
		//ns.tprint("BIOOYJAK");
		//ns.rm(runfile, "home");
		//}


		//ns.atExit(exitfn);


		if (!ns.fileExists(runfile, "home")) {
			// create runfile if it does'nt exist
			await ns.write(runfile, "running " + runfile, "w");
			ns.tprint("Created runfile: ", runfile,
				"\n\t\tDelete file to gracefully kill the script");
			//ns.tprint("run ", switchscript, " first");
		}


		var firstloop = true;
		while (ns.fileExists(runfile, "home") && firstloop || hacklevels.length > 0) {
			//loop from here
			firstloop = false;
			//myhacklevel = ns.getHackingLevel();
			hacklevels = [];
			await scanhost(basehost, null, null, 0);

			// TODO -FIXME - after each run though,
			//  if no higher level exists, end the script
			if (oneshot) break;

			if (hacklevels.length > 0) {
				let nextlevel = (hacklevels
					.sort(function (a, b) {
						return a - b; // sort ascending (smallest first)
					}))[0]; //get next hack level threshold
				ns.tprint("Next hack level: ", nextlevel, " (me: ", ns.getHackingLevel(), ")");
				while (ns.getHackingLevel() < nextlevel) {
					//  loop until my hack level increases enough
					//  so we can hack the next level
					await ns.sleep(6000);
				}
				ns.toast("Looping scanhack");
			}
		}

		ns.tprint("------ Finished Scan/installing " + recursion + " levels deep --------");
		if (ns.fileExists(runfile, "home")) await ns.rm(runfile, "home");
		//if (dryrun) ns.tprint("*** DRY RUN *** - use --force for real deal");
	} else {
		// NO TARGET specified, or bad argument
		ns.tprint(usage);
	}

	// SCAN and do things
	async function scanhost(host, ignorehost, recursioncount, indentcount) {
		await ns.sleep(10); // prevent runaway script
		if (verbose) ns.tprint(indentspace.repeat(indentcount), "SCANNING host: ", host, " ", recursioncount);
		let servers = ns.scan(host); // get servers connected to host
		while (servers.length > 0) {
			let server = servers.shift();
			if (server == ignorehost) continue; // skip if the only connected server is the one we came from
			if (exclude && server.match(exclude)) continue; // exclude filter
			if (includeonly && !server.match(includeonly)) continue; // include-only filter

			await installhack(server, indentcount);

			if (hasdeepserver(server, host, recursioncount, indentcount)) {
				// recursive scanning if deeper connection exists
				await scanhost(server, host, recursioncount + 1, indentcount + 1);
			}
		}
	}

	// check for DEEPER connection
	function hasdeepserver(nexthost, fromhost, recursioncount, indentcount) {
		if (
			recursion <= 1 || // don't look for deep servers if recursion is off
			recursioncount >= recursion // recursion limit reached
		) return false;
		let servers = ns.scan(nexthost);
		while (servers.length > 0) {
			let server = servers.shift();
			if (server != fromhost) {
				// ignore the higher host
				// proceed if there is at least one deeper connection
				//ns.tprint(indent, " going deeper (", nexthost, " : ", recursioncount + 1 , ")");
				return true;
			}
		}
		//ns.tprint(indent, " ", nexthost, " is shallow: ", ns.scan(nexthost));
		return false;
	}


	async function installhack(server, indentcount) {
		// appendonly, verbose, script____, homerambuffer, dryrun
		await ns.sleep(10);
		if (appendonly && ns.scriptRunning(scripthack, server)) {
			// don't hack if appending only to clean servers
			if (verbose) {
				ns.tprint(" ");
				ns.tprint(indent.repeat(indentcount), server, " is already running the hack scrips");
			}
			return;
		} else {
			if (verbose) ns.tprint(" ");
		}
		//if (!force && server == target) return; //skip the main target; was done first
		var info = ns.getServer(server);
		//ns.tprint(info);
		if (ns.getHackingLevel() < info.requiredHackingSkill) { //ns.getServerRequiredHackingLevel(server)) {
			if (verbose) ns.tprint(indentspace.repeat(indentcount), "-:skipping ", server, ". Requires hacking strength: ", info.requiredHackingSkill);
			hacklevels.push(info.requiredHackingSkill);
			return; //skip high hack servers
		}
		var servram = info.maxRam; //(info.maxRam - info.ramUsed); //info.maxRam;
		if (server == "home") servram = info.maxRam - homerambuffer;
		var threads = Math.floor(servram / scriptram);
		if (threads <= 0) return; // skip server if we can't run scripts
		ns.tprint(indent.repeat(indentcount), server, ": Ram:", servram, ", can run ", threads, " threads");
		// open necessary ports
		// cascading switch statement
		if (!info.hasAdminRights) {
			if (info.openPortCount < info.numOpenPortsRequired) {
				switch (info.numOpenPortsRequired) {
					case 5:
						if (ns.fileExists("SQLInject.exe", "home")) {
							ns.tprint(indentspace.repeat(indentcount), "opening SQL");
							//dryrun ? ns.tprint(indentspace.repeat(indentcount), "-dryrun-") : 
							ns.sqlinject(server);
						}
					case 4:
						if (ns.fileExists("HTTPWorm.exe", "home")) {
							ns.tprint(indentspace.repeat(indentcount), "opening HTTP");
							//dryrun ? ns.tprint(indentspace.repeat(indentcount), "-dryrun-") : 
							ns.httpworm(server);
						}
					case 3:
						if (ns.fileExists("relaySMTP.exe", "home")) {
							ns.tprint(indentspace.repeat(indentcount), "opening SMTP");
							//dryrun ? ns.tprint(indentspace.repeat(indentcount), "-dryrun-") : 
							ns.relaysmtp(server);
						}
					case 2:
						if (ns.fileExists("FTPCrack.exe", "home")) {
							ns.tprint(indentspace.repeat(indentcount), "opening FTP");
							//dryrun ? ns.tprint(indentspace.repeat(indentcount), "-dryrun-") : 
							ns.ftpcrack(server);
						}
					case 1:
						if (ns.fileExists("BruteSSH.exe", "home")) {
							ns.tprint(indentspace.repeat(indentcount), "opening SSH");
							//dryrun ? ns.tprint(indentspace.repeat(indentcount), "-dryrun-") : 
							ns.brutessh(server);
						}
					default:
				}
			}

			info = ns.getServer(server); //reload open port count
			if (info.openPortCount >= info.numOpenPortsRequired) {
				ns.tprint(indentspace.repeat(indentcount), "rooting ", server);
				//dryrun ? ns.tprint(indentspace.repeat(indentcount), "-dryrun-") : 
				ns.nuke(server);
			} else { // no root access, and not enough open ports
				ns.tprint(indentspace.repeat(indentcount), "-insufficient access. Need ",
					info.numOpenPortsRequired, " open ports");
				return;
			}
		}

		//ns.tprint(indent, "Copying script");
		//if (dryrun) {
		//	ns.tprint(indentspace.repeat(indentcount), "-dryrun-");
		//} else {
		await ns.scp(scripthack, "home", server);
		await ns.scp(scriptgrow, "home", server);
		await ns.scp(scriptweak, "home", server);

		//}
		if (threads > 0) {
			// if (!dryrun) {
			ns.tprint(indentspace.repeat(indentcount), "Executing script on server");
			//if (ns.scriptRunning(script, server)) ns.scriptKill(script, server);
			//await ns.exec(script, server, loophack, target);
			if (ns.scriptRunning(script, server)) ns.scriptKill(script, server);
			if (ns.scriptRunning(scripthack, server)) ns.scriptKill(scripthack, server);
			if (ns.scriptRunning(scriptgrow, server)) ns.scriptKill(scriptgrow, server);
			if (ns.scriptRunning(scriptweak, server)) ns.scriptKill(scriptweak, server);


			let loophack = Math.ceil(threads * 0.05);
			let loopweak = Math.floor(threads * 0.16);
			var loopgrow = threads - loophack - loopweak;
			//ns.tprint(threads + " = " + loophack + " " + loopgrow + " " + loopweak);
			if (loophack > 0) await ns.exec(scripthack, server, loophack, target);
			if (loopweak > 0) await ns.exec(scriptweak, server, loopweak, target);
			while (loopgrow > 20) {
				await ns.exec(scriptgrow, server, 20, target, loopgrow);
				loopgrow = loopgrow - 20;
			}
			if (loopgrow > 0) await ns.exec(scriptgrow, server, loopgrow, target, loopgrow);


			//} else {
			//	ns.tprint(indentspace.repeat(indentcount), "-dryrun hacking server-");
		}
	}
}