/** @param {NS} ns **/
// this script gets installed on host servers
// by the main batch-install script
import {settings, getmemtimes} from "/settings.js";

export async function main(ns) {
    var targ = ns.args[0];
    var target = ns.getServer(targ);
    var hostname = ns.getHostname();
    var host = ns.getServer(hostname);
    var moneypercent = 0.85;
    var securitythresh = 2;

    var [mem, times] = getmemtimes(ns, targ);

    var hostram = host.maxRam - mem.ctrl;

    // PART 1.1
    // Weaken the target until we achieve minimum security (minDifficulty)
		var nullarg = 0;
    while ((target.hackDifficulty > target.minDifficulty + securitythresh) || (target.moneyAvalaible < target.moneyMax * moneypercent)) {

        times = getmemtimes(ns, targ, true);

        var growdelay = times.weak + times.buff - times.grow;

        // total time to complete one cycle of this batch (weak+grow)
        var prepbatchtime = times.weak + times.buff * 2;

        // number of batch overlaps
        // ie number of batches that might run simultaneously
       // var prepsimulbatches = Math.floor(prepbatchtime / times.buff);
			 var prepsimulbatches = prepbatchtime / times.buff;

        // ram quota for each batch cycle
        var prepbatchram = hostram / prepsimulbatches;

        // number of threads each script should run
        // weak/grow have same ram requirements
        var prepthreads = Math.floor(prepbatchram / mem.weak / 2);

				/*		
				ns.tprint(
					"Server ram: " + host.maxRam + "\n"
					+ "weaken mem: "	+ mem.weak + "\n"
					+ "weaken time: "+ times.weak + "\n"
					+ "prbatchtime: "+ prepbatchtime + "\n"
				//	+ "prbatchdelay: "+ prepbatchdelay + "\n"
					+ "prsiimulbatch: "+ prepsimulbatches + "\n"
					+ "prbatchram: "	+ prepbatchram + "\n"
					+ "prepthreads: "	+ prepthreads + "\n\n");
*/

        // SEND IT
        ns.exec(settings.batch.weak, hostname, prepthreads, targ, 0, nullarg);
        ns.exec(settings.batch.grow, hostname, prepthreads, targ, growdelay, nullarg);
				nullarg++;

       
				// per-batch delay
				var prepbatchdelay = times.weak + times.buff * 2 - ns.getWeakenTime(targ);


        await ns.sleep(prepbatchdelay);
        // LOOP IT

        target = ns.getServer(targ);
				//ns.exit();
    }

    // STEP 2 -- HWGW cycle
		var nullcycle = 0;
    while (true) {
        // ns.fileExists("runfile-batchhack.txt", "home")) {
        // first set up the batch informations
        times = getmemtimes(ns, targ, true);

        var starttimes = {
            weak: times.buff * 2,
            // this is for the second weakening
            // first weaken starts with no delay
            hack: times.weak - times.buff - times.hack,
            grow: times.weak + times.buff - times.grow
        };

        // memory allocated for each batch
        var batchram = hostram / times.simulbatches;

        // denominator for ram percent calculation
        // settings.batch.allmem = mem.batchtotal

        var threads = {};
        for (const key in settings.batch) { // calculate percent of RAM used by each script
            let memper = mem[key] / mem.batchtotal;
            // then calculate number of threads per script within the batch
            threads[key] = Math.floor(batchram * memper);
        }

        // SEND IT
        // first weaken starts with no delay
        ns.exec(settings.batch.weak, hostname, threads["weak"], targ, 0, nullcycle);
        for (const key in starttimes) {
            ns.exec(settings.batch[key], hostname, threads[key], targ, starttimes[key], nullcycle);
        }
				nullcycle++;
        // per-batch delay
        // old weakentime - (new weaken time - buff * 3 )
        var batchdelay = times.weak -(ns.getWeakenTime(targ) - times.buff * 3);
        await ns.sleep(batchdelay);
        // LOOP IT
    }
}
