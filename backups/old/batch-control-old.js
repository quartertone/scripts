/** @param {NS} ns **/
// this script gets installed on host servers
// by the main batch-install script
import {settings, getmemtimes} from "/settings.js";

export async function main(ns) {
    var targ = ns.args[0];
    var target = ns.getServer(targ);
    var hostname = ns.getHostname();
    var host = ns.getServer(host);
    var moneypercent = 0.85;
    var securitythresh = 2;

    var [mem, times] = getmemtimes(ns, targ);

    var hostram = host.maxRam - mem.ctrl;

    // amount of ram to use for each script during prep stage
    var weakram = hostram * (mem.weak / (mem.weak + mem.grow));
    var growram = hostram * (mem.grow / (mem.weak + mem.grow));

    // same for grow and weaken
    var initialthreads = Math.floor((hostram - mem.ctrl) / mem.weak);
    //    var growthreads = Math.floor(hostram * (mem.grow / (mem.weak + mem.grow)));

    // PART 1.1
    // Weaken the target until we achieve minimum security (minDifficulty)
    while ((target.hackDifficulty > target.minDifficulty + securitythresh) && (target.moneyAvalaible<target.moneyMax * moneypercent)) {
    	while (target.hackDifficulty>target.minDifficulty + securitythresh) {
        var weakentime = ns.getWeakenTime(targ);

        // this part is inefficient... fix it maybe?
        if (! ns.scriptRunning(settings.batch.weak, hostname)) {
            ns.exec(settings.batch.weak, hostname, initialthreads, targ, 0);
            await ns.sleep(weakentime + times.buff);
        }
        target = ns.getServer(targ);
        // update info before next loop
    }
    // PART 1.2
    // grow/weaken to max money and min security
        while (target.moneyAvalaible < target.moneyMax * moneypercent) {
        var growtime = ns.getGrowTime(targ);
        // var growthsecurityincrease = ns.growthAnalyzeSecurity(growthreads);

        if (! ns.scriptRunning(settings.batch.grow, hostname)) {
            ns.exec(settings.batch.grow.file, hostname, initialthreads, targ, 0);
            await ns.sleep(growtime + times.buff);
        }
        target = ns.getServer(targ); // update info
    }
    // we have the max moneys
}

// STEP 2 -- HWGW cycle
while (true) {
    // ns.fileExists("runfile-batchhack.txt", "home")) {
    // first set up the batch informations
    times = getmemtimes(ns, targ, true);

    var starttimes = {
        weak: times.buff * 2,
        // this is for the second weakening
        // first weaken starts with no delay
        hack: times.weak - times.buff - times.hack,
        grow: times.weak + times.buff - times.grow
    };

    // memory allocated for each batch
    var batchram = hostram / times.simulbatches;

    // denominator for ram percent calculation
    // settings.batch.allmem = mem.batchtotal

    var threads = {};
    for (const key in settings.batch) { // calculate percent of RAM used by each script
        let memper = mem[key] / mem.batchtotal;
        // then calculate number of threads per script within the batch
        threads[key] = Math.floor(batchram * memper);
    }

    // SEND IT
    // first weaken starts with no delay
    ns.exec(settings.batch.weak, hostname, threads["weak"], targ, 0);
    for (const key in starttimes) {
        ns.exec(settings.batch[key], hostname, threads[key], targ, starttimes[key]);
    }

    // per-batch delay
    // old weakentime - (new weaken time - buff * 3 )
    var batchdelay = times.weak -(ns.getWeakenTime(targ) - times.buff * 3);
    await ns.sleep(batchdelay);
    // LOOP IT
}}
