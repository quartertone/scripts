var ram = 32; //ns.getPurchasedServerMaxRam();

//ns.tprint(ram);
var script = "hack.ns";
var target = args[0];
var delserv = true;
var i = 0;
while (i < getPurchasedServerLimit()) {
	var servname = "pserv-" + i;
	if (getServerMoneyAvailable("home") > getPurchasedServerCost(ram)) {
		if (serverExists(servname) && delserv) {
			killall(servname);
			tprint("deleting ", servname);
			deleteServer(servname);
		} else if (serverExists(servname)) {
			servname = "pserv-" + ++i;
		}

		var server = purchaseServer(servname, ram);
		var scriptram = getScriptRam(script, "home");
		//ns.tprint(script, " mem ", scriptram);
		var servram = getServerMaxRam(server);
		var threads = Math.floor(servram / scriptram);
		//ns.tprint(server, ": Ram:", servram, ", can run ", threads, " threads");
		//ns.tprint("Copying script");
		scp(script, "home", servname);
		if (threads > 0) {
			//ns.tprint("Executing script on server");
			//	if (ns.scriptRunning(script, server)) ns.scriptKill(script, server);
			tprint(servname, " Ram:", servram, "/", ram, " threads:", threads);
			exec(script, servname, threads, target);
		}
	}
	++i;
}
tprint("--end--");