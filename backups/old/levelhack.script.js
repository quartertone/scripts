if (args.length > 0) {
//	var host = args[0]; // ? args[0] : getHostname();
	var target = args[0];
	var doit = (args[1] == "doit");
	var script = "hack.script";
	var scriptram = getScriptRam(script, "home");

	//tprint(script, " mem ", scriptram);

	// do the target first
	scanhack(target);


function iteratescan (host) {
	var servers = scan(host);
	for (i = 0; i < servers.length; i++) {
		//scanhack(servers[i]);
		//if (servers[1] != host)
		//var nextlevel = scan(servers[1]);
		
		iteratescan(servers[1]);
	}


}


/*
scan home host
- scan 2nd level hosts
- eliminate servers with no connections other than home
- scan remaining hosts, and installl scripts
- repeat until no more servers


*/



	tprint("------ The End --------");

} else {
	tprint(" need arguments: [host] [target] {doit} ");

}


function scanhack(server) {
	var servram = getServerMaxRam(server);
	var threads = Math.floor(servram / scriptram);
	tprint("----------------");
	tprint(server, ": Ram:", servram, 
		", can run ", threads, " threads");


	if (doit) {
		if (fileExists("BruteSSH.exe", "home")) brutessh(server);
		tprint("opening SSH");

		if (fileExists("FTPCrack.exe", "home")) ftpcrack(server);
		tprint("opening FTP");

		if (!hasRootAccess(server)) nuke(server);
		tprint("rooting");

		scp(script, "home", server);
		tprint("copying");

		if (threads > 0) exec(script, server, numThreads = threads, target);
		tprint("executing script on server");
	}

}