/** @param {NS} ns **/
export async function main(ns) {


	var servers = ns.getPurchasedServers();


	var lines = ns.args;
	var force = false;
	var target = "";
	while (lines.length > 0) {
		let item = lines.shift();
		switch (item) {
			case "--delete": // specify host; default = "home"
				force = true;
				break;
			case "-s": // specify server; this or plain "target" required
				target = lines.shift();
				break;
			default:
		}
	}


	while (servers.length > 0) {
		var server = servers.shift();

		if (!target || server.match(target)) {
			ns.tprint(server);
			if (force) {
				ns.killall(server);
				ns.deleteServer(server);
				ns.tprint("Deleted ", server);
			}
		}

	}


}