if (args.length > 0) {
	var host = args[0]; // ? args[0] : getHostname();
	var target = args[1];
	var doit = (args[2] == "doit");
	var script = "hack.script";
	var scriptram = getScriptRam(script, "home");

	tprint(script, " mem ", scriptram);

	// do the target first
	if (host == "home") scanhack(target, true);

	var servers = scan(host);
	for (i = 0; i < servers.length; i++) {
		scanhack(servers[i]);
	}



	tprint("------ The End --------");

} else {
	tprint(" need arguments: [host] [target] {doit} ");

}


function scanhack(server, force) {
	var servram = getServerMaxRam(server);
	var threads = Math.floor(servram / scriptram);
	if ( !force && (server == "home") || (server == target)) return;

	tprint("----------------");
	tprint(server, ": Ram:", servram, 
		", can run ", threads, " threads");


	if (doit) {
		if (fileExists("BruteSSH.exe", "home")) brutessh(server);
		tprint("opening SSH");

		if (fileExists("FTPCrack.exe", "home")) ftpcrack(server);
		tprint("opening FTP");

		if (fileExists("relaySMTP.exe"), "home")) relaysmtp(server);
		tprint("opening SMTP");

		if (!hasRootAccess(server)) nuke(server);
		tprint("rooting");

		scp(script, "home", server);
		tprint("copying");

		if (scriptRunning(script, server)) scriptKill(script, server);

		if (threads > 0) exec(script, server, numThreads = threads, target);
		tprint("executing script on server");
	}

}